# PyHVWrapper

This library provides a basic support of the CAEN HVWrapper library ported for python. It's intended to be used
for simple operations like monitoring the parameters of the channels and control them. It's not intended to be
a one to one port of the HVWRapper library, so most of the functions are not implemented.

# API

To connect to a module you have to provide the path to the HVWrapper dynamic library and the ip address with credentials to
the module. You also have to be sure that the module is connected to the same network where the python script is run

When using `get_param()`, to get a parameter, the returned data is a python dict where they key is the channel id, and the value is the parameter.


## Example: connecting to a mainframe on the same network

From the CAENHVWrapper API documentation, the system depends on the model. Assuming SY5527, it will be 3. The `link_type` will be 0, and `connection_arg` will be the IP address as a string. If the device is registered on the network, you can use the `socket` module to get the IP address from the DNS name.

```python
from pyhvwrapper.HVWrapper import HV
import socket

hostname = 'hvmainframeexample.cern.ch'
ip_address = socket.gethostbyname(hostname)
hv = HV(connection_arg=ip_address, sys_type=3, 
        user='user', password='password')

# connect to the module
hv.connect()

# get the create map as a dict
print(hv.crate_map) # returns a dict of the crate/channels map

# get the name of the channels
print(hv.get_ch_name(slot=0, channels=2)) # {2: "CH2"}
print(hv.get_ch_name(slot=0, channels=[0, 1, 2, 3]))# {0: "CH0", 1: "CH1", 2: "CH2", 3: "CH3"}

# get the value of a parameter
print(hv.get_param(slot=0, channels=2, param='Status') ) # returns {2: String}
print(hv.get_param(slot=0, channels=2, param='Status', return_code=True) ) # returns {2: int}
print(hv.get_param(slot=0, channels=[2, 1], param='V0Set'))  # returns {1: float, 2: float}

# set a parameter
hv.set_param(slot=0, channels=2, param='V0Set', value=100) # return the status code from executing the HVWrapper command (0 = okay)
hv.set_param(slot=0, channels=[2,3], param='Pw', value=1) # return the status code from executing the HVWrapper command (0 = okay)

# disconnect from the module when you are done doing operation
hv.disconnect()
```

## Example: connecting to an NIM USB board

From the CAENHVWrapper API documentation, `link_type` should be set to 3 and `connection_arg` should be set to 0. The system depends on the board. Assuming to use 
a `N1570`, it will be 6.

```python
from pyhvwrapper.HVWrapper import HV

hv = HV(connection_arg=0, sys_type=6, link_type=3,
        user='user', password='password')

# connect to the module
hv.connect()

# get the create map as a dict
print(hv.crate_map) # returns a dict of the crate/channels map

# get the name of the channels
print(hv.get_ch_name(slot=0, channels=2)) # {2: "CH2"}
print(hv.get_ch_name(slot=0, channels=[0, 1, 2, 3]))# {0: "CH0", 1: "CH1", 2: "CH2", 3: "CH3"}

# get the value of a parameter
print(hv.get_param(slot=0, channels=2, param='Status') ) # returns {2: String}
print(hv.get_param(slot=0, channels=2, param='Status', return_code=True) ) # returns {2: int}
print(hv.get_param(slot=0, channels=[2, 1], param='V0Set'))  # returns {1: float, 2: float}

# set a parameter
hv.set_param(slot=0, channels=2, param='V0Set', value=100) # return the status code from executing the HVWrapper command (0 = okay)
hv.set_param(slot=0, channels=[2,3], param='Pw', value=1) # return the status code from executing the HVWrapper command (0 = okay)

# disconnect from the module when you are done doing operation
hv.disconnect()
```