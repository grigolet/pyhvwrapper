import ctypes as c
import logging
from .utils import error_codes, param_types, status_types
from pathlib import Path
import sys

if sys.platform == 'win32':
    lib_path = Path(__file__).parents[0] / 'lib' / \
        'x64' / 'CAENHVWrapper.dll'
else:
    lib_path = Path(__file__).parents[0] / 'lib' / \
        'x64' / 'libcaenhvwrapper.so.6.3'

logging.basicConfig()


class HV:
    """This class provides the basic API to login/logout from an HV module.
    It provides also the api to read an arbitrary parameter and to set a value.
    The class is configured to acces the HV module only using TCP/IP"""
    MAX_CH_NAME = 12  # from library headers

    def __init__(self, connection_arg, sys_type, user, password, lib_path=lib_path, logger=None,
                 link_type=0,
                 *args, **kwargs):
        self.connection_arg = connection_arg
        self.sys_type = sys_type
        self.user = user
        self.password = password
        self.handle_value = c.c_int(-1)
        self.link_type = link_type
        self.handle = c.pointer(self.handle_value)
        self.hvlib = c.cdll.LoadLibrary(str(lib_path))
        self.logger = logger or logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.logger.debug("Using lib_path at %s", lib_path)
        self.last_result_code = None

    def connect(self):
        """Login to the HV module. Return the CAEN code associated with the
        return of the CAENHV_InitSystem function."""
        system = c.c_int(self.sys_type)
        link_type = c.c_int(self.link_type)
        connection_arg = c.c_int(0) if self.connection_arg == 0 else c.c_char_p(
            str.encode(self.connection_arg))
        user = c.c_char_p(str.encode(self.user))
        password = c.c_char_p(str.encode(self.password))
        init = self.hvlib.CAENHV_InitSystem
        self.logger.debug(
            "Created C variable to handle connection to the system. Running C init function")
        result = init(system, link_type,
                      connection_arg, user, password, self.handle)
        self.logger.debug(
            "C init function of HVWrapper returned with result %s", result)
        self.last_result_code = result
        self.logger.debug(
            f'CAENHV_InitSystem: {error_codes[result]} with handle {self.handle_value}')
        # if result is okay get crate map
        if result == 0:
            self.get_map()
            # construct also the channels map: num => name
            channels_map = {}
            for slot in self.crate_map.keys():
                channels_num = self.crate_map[slot]['channels']
                channels_map = self.get_ch_name(
                    slot, list(range(channels_num)))
                # construct also the inverse channel map: name => num
                self.crate_map[slot]['channels_map'] = channels_map
                inverse_channels_map = {v: k for k, v in channels_map.items()}
                self.crate_map[slot]['inverse_channels_map'] = inverse_channels_map
        return result

    def disconnect(self):
        """Logout from the HV module and returns the output from the CAEN_DeinitSystem"""
        deinit = self.hvlib.CAENHV_DeinitSystem
        result = deinit(self.handle_value)
        self.last_result_code = result
        self.logger.debug(f'CAENHV_Deinitsystem: {error_codes[result]}')
        return result

    def get_map(self):
        """Return a dict containing information about the map of the create.
        The map has this structure: {
            <slot_number>: {
                'channels': <number_of_channels>,
                'board': <serial_number_of_the_board>,
                'desc': <description_of_the_board>
            }
        }"""
        # initialize the variables for executin the C function
        self.crate_map = {}
        slot_num = c.c_ushort()
        channels_map = c.pointer(c.c_ushort())
        board_list = c.pointer(c.create_string_buffer(1000))
        desc_list = c.pointer(c.create_string_buffer(1000))
        serial_num_list = c.pointer(c.c_ushort())
        FmwRelMinList = c.pointer(c.pointer(c.c_ubyte()))
        FmwRelMaxList = c.pointer(c.pointer(c.c_ubyte()))
        get_map = self.hvlib.CAENHV_GetCrateMap
        # execute the C function
        self.logger.debug("Handle value: %s", self.handle_value)
        result = get_map(self.handle_value, c.byref(slot_num),
                         c.byref(channels_map), c.byref(
                             board_list), c.byref(desc_list),
                         serial_num_list, FmwRelMinList, FmwRelMaxList)
        self.last_result_code = result
        self.logger.debug(f'CAENHV_GetCrateMap result: {result} - {error_codes[result]}')
        # I don't know why but pointer of a buffer passed by ref is the only
        # solution I find to use this function
        board_list_buffer = board_list[0]
        desc_list_buffer = desc_list[0]
        board_ix_start = 0
        desc_ix_start = 0
        # if the result is okay construct a dict of the channels and boards
        if result == 0:  # meaning that command is executely properly
            for slot in range(slot_num.value):
                if board_list_buffer[board_ix_start] == b"\x00":
                    # empty string: no board present
                    board_ix_start += 1
                    desc_ix_start += 1
                    continue
                else:
                    # Parse string data for board and description
                    board_ix_stop = board_list_buffer[:].index(
                        b"\x00", board_ix_start) + 1  # +1 needed to index the
                    # correct position of null terminator character
                    # -1 needed to parse \x00 into the string
                    board_str = board_list_buffer[board_ix_start:board_ix_stop-1]
                    board_ix_start = board_ix_stop

                    desc_ix_stop = desc_list_buffer[:].index(
                        b"\x00", desc_ix_start) + 1
                    desc_str = desc_list_buffer[desc_ix_start:desc_ix_stop-1]
                    desc_ix_start = desc_ix_stop

                channels = channels_map[slot]
                try:

                    board = board_str.decode('utf8')
                    desc = desc_str.decode('utf8')
                except Exception as e:
                    self.logger.warning(e)
                    board = ''
                    desc = ''
                self.crate_map[slot] = {
                    'channels': channels,
                    'board': board,
                    'desc': desc
                }

            self.hvlib.CAENHV_Free(board_list)
            self.hvlib.CAENHV_Free(desc_list)
            self.hvlib.CAENHV_Free(channels_map)

        return result

    def check_slot_and_channel(self, slot, channels):
        # additional check for channels/slot correspondance
        # - check if slot is in crate map
        if slot not in self.crate_map:
            raise KeyError(f'Slot {slot} is not in crate map')
        # first, check if it's a list of integer
        are_ints = all(isinstance(channels, int) for channel in channels)
        # or if it is a list of strings
        are_strings = all(isinstance(channels, str) for channel in channels)
        if are_ints:
            # check if there is any channel inside channesl that is not
            # in the channels of the asked slot
            if any(map(lambda x: x not in self.crate_map[slot]['channels_map'], channels)):
                raise KeyError(
                    f'Channels {channels} not found for the slot {slot}')
        elif are_strings:
            if any(map(lambda x: x not in self.crate_map[slot]['inverse_channels_map'], channels)):
                raise KeyError(
                    f'Channels {channels} not found for the slot {slot}')

    def string_channels_to_int(self, slot, channels):
        return [self.crate_map[slot]['inverse_channels_map'][channel] for channel in channels]

    def _are_strings(self, channels):
        return all(isinstance(channel, str) for channel in channels)

    def check_valid_param_name(self, param):
        # check that the parameter is valid
        if param not in param_types.keys():
            raise KeyError(f'Parameter {param} not implemented or invalid')

    def get_ch_name(self, slot, channels, return_result=False):
        """Return the channel names"""
        # If channels is only one transform into a list
        if isinstance(channels, (int, str)):
            channels = [channels]
        self.check_slot_and_channel(slot, channels)
        slot = c.c_ushort(slot)
        ch_num = c.c_ushort(len(channels))
        # this is the ctypes representation of a list to pointer of ushort
        ch_list = (c.c_ushort * len(channels))(*channels)
        # this is the representation of an array of fixed size chars
        ch_names = ((c.c_char * self.MAX_CH_NAME) * len(channels))()
        get_names = self.hvlib.CAENHV_GetChName
        result = get_names(self.handle_value, slot, ch_num, ch_list, ch_names)
        self.last_result_code = result
        self.logger.debug(f'CAENHV_GetChName: {error_codes[result]}')
        # construct a dict with key the number of the channel and value the
        # name of the channel
        channels_map = {}
        for ix, channel in enumerate(channels):
            channels_map[channel] = ch_names[ix].value.decode('utf8')

        if return_result:
            return channels_map, result
        return channels_map

    def get_param(self, slot, channels, param, return_code=False):
        """Return the parameter from a particular slot and channel."""
        if isinstance(channels, (int, str)):
            channels = [channels]
        self.check_valid_param_name(param)
        self.check_slot_and_channel(slot, channels)
        if self._are_strings(channels):
            int_channels = self.string_channels_to_int(slot, channels)
        else:
            int_channels = channels
        get_ch_param = self.hvlib.CAENHV_GetChParam
        c_slot = c.c_ushort(slot)
        par_name = param.encode('utf8')
        ch_num = c.c_ushort(len(int_channels))
        ch_list = (c.c_ushort * len(int_channels))(*int_channels)
        par_var_list = (param_types[param] * len(int_channels))(*int_channels)
        result = get_ch_param(self.handle_value, c_slot,
                              par_name, ch_num, ch_list, c.byref(par_var_list))
        self.last_result_code = result
        self.logger.debug(
            f'CAENHV_GetChParam: {error_codes[result]} with handle {self.handle_value}')
        # consutrct a map of the results. Only if the type is results
        # return the corresponding message
        params = {}
        for ix, channel in enumerate(channels):
            value = par_var_list[ix]
            if param == 'Status' and not return_code:
                if par_var_list[ix] in status_types:
                    value = status_types[par_var_list[ix]]
                else:
                    self.logger.debug(
                        f'Key {par_var_list[ix]} missing in status_types')
                    value = "Undefined"

            params[channel] = value

        return params

    def set_param(self, slot, channels, param, value):
        """Set a parameter for a channels of a slot to a value"""
        if isinstance(channels, (int, str)):
            channels = [channels]
        self.check_valid_param_name(param)
        self.check_slot_and_channel(slot, channels)
        if self._are_strings(channels):
            int_channels = self.string_channels_to_int(slot, channels)
        else:
            int_channels = channels
        set_param = self.hvlib.CAENHV_SetChParam
        ch_num = c.c_ushort(len(int_channels))
        ch_list = (c.c_ushort * len(int_channels))(*int_channels)
        par_name = param.encode('utf8')
        par_value = c.byref(param_types[param](value))
        result = set_param(self.handle_value, slot, par_name,
                           ch_num, ch_list, par_value)
        self.last_result_code = result
        self.logger.debug(
            f'CAENHV_SetChParam: {error_codes.get(result, result)} with handle {self.handle_value}')

        return result
