import io
import os
import re

from setuptools import find_packages
from setuptools import setup


def read(filename):
    filename = os.path.join(os.path.dirname(__file__), filename)
    text_type = type(u"")
    with io.open(filename, mode="r", encoding='utf-8') as fd:
        return re.sub(text_type(r':[a-z]+:`~?(.*?)`'), text_type(r'``\1``'), fd.read())


setup(
    name="pyhvwrapper",
    version="0.2.0",
    url="https://gitlab.cern.ch/grigolet/pyhvwrapper",
    license='MIT',

    author="Gianluca Rigoletti",
    author_email="gianluca.rigoletti@cern.ch",

    description="High level subset of CAEN's HVWrapper functions written in python",
    long_description=read("README.md"),

    packages=find_packages(exclude=('tests',)),

    install_requires=[],
    package_data={
        'pyhvwrapper': ['lib/*.*', 'lib/*/*.*']
    },
    include_package_data=True,

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
