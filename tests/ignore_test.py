import logging
from HVWrapper import HV

if __name__ == "__main__":
    import time
    logger = logging.StreamHandler()
    logger.setLevel(logging.DEBUG)
    ip_address_256 = '128.141.217.236'
    ip_address_ecogas = '128.141.151.206'
    ip_address_gif = '128.141.149.246'
    sys_type_gif = 3 # SY5527
    sys_type_ecogas = 0 # 1527
    user = 'admin'
    password = 'admin'
    lib_path = 'lib/x64/libcaenhvwrapper.so.5.82'
    hv = HV(ip_address_gif, sys_type_gif, user, password, lib_path)
    hv.connect()
    time.sleep(0.5)
    logging.info('Waiting some time')
    time.sleep(1)
    print(hv.crate_map)
    time.sleep(1)
    print(hv.get_param(slot=0, channels='RPC0', param='Status'))
    print(hv.get_param(slot=0, channels=2, param='Status'))
    hv.set_param(slot=0, channels=2, param='V0Set', value=100)
    hv.set_param(slot=0, channels=[2,3], param='Pw', value=1)
    time.sleep(2)
    hv.set_param(slot=0, channels="CHANNEL2", param='Pw', value=1)
    # print(hv.get_ch_name(slot=0, channels=2))
    # print(hv.get_ch_name(slot=0, channels=[0, 1, 2, 3]))
    # print(hv.get_ch_name(slot=0, channels=22))
    time.sleep(0.5)
    hv.disconnect()
