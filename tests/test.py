import unittest
from HVWrapper import HV
import logging
import json

class TestHV(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logger = logging.StreamHandler()
        logger.setLevel(logging.DEBUG)
        with open('configs.json') as f:
            configs = json.load(f)
        cls.hv = HV(configs['ip'], configs['sys_type'], configs['user'], configs['password'], configs['lib_path'])
        cls.hv.connect()

    def test_connection(self):
        result = self.hv.connect()
        self.assertEqual(result, 0)

    def test_crate_map(self):
        crate_map = {0: {'channels': 6, 'board': 'A1526', 'desc': ' 6 Ch Neg. 15KV 1mA  '}}
        self.assertEqual(self.hv.crate_map, crate_map)

    def test_ch_name_int(self):
        result = self.hv.get_ch_name(slot=0, channels=2)
        self.assertEqual(result, {2: 'CHANNEL02'})

    def test_ch_name_list(self):
        result = self.hv.get_ch_name(slot=0, channels=[0, 1, 2, 3])
        expected = {0: 'RPC0', 1: 'RPC1', 2: 'CHANNEL02', 3: 'CHANNEL03'}
        self.assertEqual(result, expected)

    def test_ch_name_error_channel(self):
        with self.assertRaises(KeyError):
            self.hv.get_ch_name(slot=0, channels=22)
    
    def test_ch_name_error_module(self):
        with self.assertRaises(KeyError):
            self.hv.get_ch_name(slot=100, channels=1)

    def test_get_param(self):
        result = self.hv.get_param(slot=0, channels=0, param='V0Set')
        all_float = all(map(lambda x: isinstance(x, float), result))
        self.assertTrue(all_float)


    @classmethod
    def tearDownClass(cls):
        cls.hv.disconnect()

if __name__ == "__main__":
    unittest.main()